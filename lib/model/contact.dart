class Contact {
  String name;
  String avatar;
  String gender;
  String phone;
  String email;
  String id;

  Contact({
    required this.name,
    required this.avatar,
    required this.gender,
    required this.phone,
    required this.email,
    required this.id,
  });

  factory Contact.fromJson(Map<String, dynamic> json) => Contact(
        name: json["name"],
        avatar: json["avatar"],
        gender: json["gender"],
        phone: json["phone"],
        email: json["email"],
        id: json["id"],
      );

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'avatar': avatar,
      'gender': gender,
      'phone': phone,
      'email': email,
      'id': id,
    };
  }

  // Map<String, dynamic> toJson() => {
  //     "name": name,
  //     "avatar": avatar,
  //     "gender": gender,
  //     "phone": phone,
  //     "email": email,
  //     "id": id,
  // };
}
