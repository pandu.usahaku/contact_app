import 'package:flutter/material.dart';

class ContactListItem extends StatelessWidget {
  final String avatarImg;
  final String name;
  final String phoneNumber;

  const ContactListItem(
      {super.key,
      required this.avatarImg,
      required this.name,
      required this.phoneNumber});

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return Card(
      child: SizedBox(
        height: screenHeight / 10,
        child: Row(children: [
          Flexible(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: ClipOval(
                child: Image.network(
                  avatarImg,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight / 30,
                  child: Text(name),
                ),
                SizedBox(
                  height: screenHeight / 30,
                  child: Text(phoneNumber),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
