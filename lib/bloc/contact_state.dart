part of 'contact_bloc.dart';

abstract class ContactState extends Equatable {
  const ContactState();

  @override
  List<Object> get props => [];
}

class ContactInitial extends ContactState {}

class ContactLoading extends ContactState {}

class ContactLoaded extends ContactState {
  final List<Contact> contacts;
  final String source;

  const ContactLoaded(this.contacts, this.source);

  @override
  List<Object> get props => [contacts, source];
}

class ContactError extends ContactState {
  final String errorMessage;

  const ContactError(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}

class ContactDetailLoading extends ContactState {}

class ContactDetailLoaded extends ContactState {
  final Contact contact;

  const ContactDetailLoaded(this.contact);

  @override
  List<Object> get props => [contact];
}

class ContactDetailError extends ContactState {
  final String errorMessage;

  const ContactDetailError(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}

class SearchLoading extends ContactState {}

class SearchLoaded extends ContactState {
  final List<Contact> contacts;

  const SearchLoaded(this.contacts);

  @override
  List<Object> get props => [contacts];
}

class SearchError extends ContactState {
  final String errorMessage;

  const SearchError(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}

class ContactDetailByEmailLoading extends ContactState {}

class ContactDetailByEmailLoaded extends ContactState {
  final List<Contact> contact;

  const ContactDetailByEmailLoaded(this.contact);

  @override
  List<Object> get props => [contact];
}

class ContactDetailByEmailError extends ContactState {
  final String errorMessage;

  const ContactDetailByEmailError(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}

class FilterLoading extends ContactState {}

class FilterLoaded extends ContactState {
  final List<Contact> contacts;

  const FilterLoaded(this.contacts);

  @override
  List<Object> get props => [contacts];
}

class FilterError extends ContactState {
  final String errorMessage;

  const FilterError(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];
}
