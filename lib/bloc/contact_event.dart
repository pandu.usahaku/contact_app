part of 'contact_bloc.dart';

abstract class ContactEvent extends Equatable {
  const ContactEvent();

  @override
  List<Object> get props => [];
}

class FetchContact extends ContactEvent {}

class FetchMoreContact extends ContactEvent {}

class SetLoadingData extends ContactEvent {
  final bool isLoading;

  const SetLoadingData(this.isLoading);

  @override
  List<Object> get props => [isLoading];
}

class FetchContactById extends ContactEvent {
  final String id;
  final String source;

  const FetchContactById({required this.id, required this.source});

  @override
  List<Object> get props => [id];
}

class FetchContactByEmail extends ContactEvent {
  final String email;

  const FetchContactByEmail(this.email);

  @override
  List<Object> get props => [email];
}

class SearchContactByName extends ContactEvent {
  final String name;

  const SearchContactByName(this.name);

  @override
  List<Object> get props => [name];
}

class FilterContactByGender extends ContactEvent {
  final String gender;

  const FilterContactByGender(this.gender);

  @override
  List<Object> get props => [gender];
}
