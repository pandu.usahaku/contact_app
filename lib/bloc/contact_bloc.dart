import 'package:bloc/bloc.dart';
import 'package:contact_app/model/contact.dart';
import 'package:contact_app/repository/api.dart';
import 'package:contact_app/repository/contact_data.dart';
import 'package:equatable/equatable.dart';

part 'contact_event.dart';
part 'contact_state.dart';

class ContactBloc extends Bloc<ContactEvent, ContactState> {
  final ContactData _contactData;
  List<Contact> _firstPageContacts = [];
  final List<Contact> _allContacts = [];
  int _currentPage = 1;
  bool _hasReachedMax = false;
  bool _loadingData = false;
  String source = '';

  ContactBloc(this._contactData) : super(ContactInitial()) {
    on<FetchContact>((event, emit) => _onFetchContact(emit));
    on<FetchMoreContact>((event, emit) => _onFetchMoreContact(emit));
    on<SetLoadingData>((event, emit) => _loadingData = event.isLoading);
    on<FetchContactById>(
        (event, emit) => _onFetchContactById(emit, event.id, event.source));
    on<SearchContactByName>(
        (event, emit) => _onSearchContact(emit, event.name));
    on<FetchContactByEmail>(
        (event, emit) => _onContactDetailByEmail(emit, event.email));
    on<FilterContactByGender>(
        (event, emit) => _onFilterContact(emit, event.gender));
  }

  bool get loadingData => _loadingData;

  Future<void> _onFetchContact(Emitter<ContactState> emit) async {
    try {
      emit(ContactLoading());

      final List<Contact> contacts =
          await _contactData.fetchContact(Api.baseUrl1, Api.personData1, 1);

      _firstPageContacts = contacts;

      emit(ContactLoaded(_firstPageContacts, source = 'source1'));
    } catch (e) {
      emit(const ContactError('Failed to load contacts'));
    }
  }

  Future<void> _onFetchMoreContact(Emitter<ContactState> emit) async {
    try {
      if (!_hasReachedMax) {
        _currentPage++;

        List<Contact> moreContacts;

        final String currentSource = _determineCurrentSource(_currentPage);

        if (_currentPage <= 5) {
          moreContacts = await _contactData.fetchContact(
              Api.baseUrl1, Api.personData1, _currentPage);
        } else if (_currentPage > 5 && _currentPage <= 10) {
          moreContacts = await _contactData.fetchContact(
              Api.baseUrl2, Api.personData2, _currentPage - 5);
        } else if (_currentPage > 10 && _currentPage <= 15) {
          moreContacts = await _contactData.fetchContact(
              Api.baseUrl3, Api.personData3, _currentPage - 10);
        } else {
          moreContacts = [];
        }

        if (moreContacts.isEmpty) {
          _hasReachedMax = true;
        } else {
          _allContacts.addAll(moreContacts);

          final List<Contact> combinedContacts = List.from(_firstPageContacts)
            ..addAll(_allContacts);
          emit(ContactLoaded(combinedContacts, currentSource));
        }

        _loadingData = false;
      }
    } catch (e) {
      emit(const ContactError('No more contact available'));
    }
  }

  String _determineCurrentSource(int currentPage) {
    if (currentPage <= 5) {
      source = 'source1';
    } else if (currentPage > 5 && currentPage <= 10) {
      source = 'source2';
    } else if (_currentPage > 10 && _currentPage <= 15) {
      source = 'source3';
    }

    return source;
  }

  Future<void> _onFetchContactById(
      Emitter<ContactState> emit, String id, String source) async {
    try {
      emit(ContactDetailLoading());

      Contact contactDetail;

      switch (source) {
        case 'source1':
          contactDetail = await _contactData.fetchContactById(
              Api.baseUrl1, Api.personData1, id);
          break;
        case 'source2':
          contactDetail = await _contactData.fetchContactById(
              Api.baseUrl2, Api.personData2, id);
          break;
        case 'source3':
          contactDetail = await _contactData.fetchContactById(
              Api.baseUrl3, Api.personData3, id);
          break;
        default:
          throw Exception('Invalid source');
      }

      emit(ContactDetailLoaded(contactDetail));
    } catch (e) {
      emit(const ContactDetailError('Failed to load contact detail'));
    }
  }

  Future<void> _onSearchContact(Emitter<ContactState> emit, String name) async {
    try {
      emit(SearchLoading());

      final List<Contact> searchResultsFromSource1 = await _contactData
          .searchContactByName(Api.baseUrl1, Api.searchPerson1, name);

      final List<Contact> searchResultsFromSource2 = await _contactData
          .searchContactByName(Api.baseUrl2, Api.searchPerson2, name);

      final List<Contact> searchResultsFromSource3 = await _contactData
          .searchContactByName(Api.baseUrl3, Api.searchPerson3, name);

      List<Contact> contactSearchResults = [];

      if (searchResultsFromSource1.isNotEmpty) {
        contactSearchResults.addAll(searchResultsFromSource1);
      }
      if (searchResultsFromSource2.isNotEmpty) {
        contactSearchResults.addAll(searchResultsFromSource2);
      }
      if (searchResultsFromSource3.isNotEmpty) {
        contactSearchResults.addAll(searchResultsFromSource3);
      }

      if (contactSearchResults.isEmpty) {
        emit(const SearchError('No results found'));
      } else {
        emit(SearchLoaded(contactSearchResults));
      }
    } catch (e) {
      emit(throw Exception(e));
    }
  }

  Future<void> _onContactDetailByEmail(
      Emitter<ContactState> emit, String email) async {
    try {
      emit(ContactDetailByEmailLoading());

      final List<Contact> resultsFromSource1 = await _contactData
          .fetchContactByEmail(Api.baseUrl1, Api.emailPerson1, email);

      final List<Contact> resultsFromSource2 = await _contactData
          .fetchContactByEmail(Api.baseUrl2, Api.emailPerson2, email);

      final List<Contact> resultsFromSource3 = await _contactData
          .fetchContactByEmail(Api.baseUrl3, Api.emailPerson3, email);

      List<Contact> contactResults = [];

      if (resultsFromSource1.isNotEmpty) {
        contactResults.addAll(resultsFromSource1);
      }
      if (resultsFromSource2.isNotEmpty) {
        contactResults.addAll(resultsFromSource2);
      }
      if (resultsFromSource3.isNotEmpty) {
        contactResults.addAll(resultsFromSource3);
      }

      if (contactResults.isEmpty) {
        emit(const ContactDetailByEmailError('No results found'));
      } else {
        emit(ContactDetailByEmailLoaded(contactResults));
      }
    } catch (e) {
      emit(throw Exception(e));
    }
  }

  Future<void> _onFilterContact(
      Emitter<ContactState> emit, String gender) async {
    try {
      emit(FilterLoading());

      List<Contact> contactFilterResults;

      final List<Contact> filterResultsFromSource1 = await _contactData
          .filterContactByGender(Api.baseUrl1, Api.filterPerson1, gender);

      final List<Contact> filterResultsFromSource2 = await _contactData
          .filterContactByGender(Api.baseUrl2, Api.filterPerson2, gender);

      final List<Contact> filterResultsFromSource3 = await _contactData
          .filterContactByGender(Api.baseUrl3, Api.filterPerson3, gender);

      contactFilterResults = filterResultsFromSource1 +
          filterResultsFromSource2 +
          filterResultsFromSource3;

      emit(FilterLoaded(contactFilterResults));
    } catch (e) {
      emit(const FilterError('Failed to filter contact'));
    }
  }
}
