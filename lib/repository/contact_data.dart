import 'package:contact_app/model/contact.dart';
import 'package:contact_app/repository/api.dart';
import 'package:dio/dio.dart';

class ContactData {
  var dio = Dio();

  Future<List<Contact>> fetchContact(
      String baseUrl, String endpoint, int page) async {
    try {
      dio.options.headers[Api.headerType] = Api.headerValue;

      final response = await dio.get('$baseUrl$endpoint',
          queryParameters: {'limit': 20, 'page': page});

      if (response.statusCode == 200) {
        List<dynamic> data = response.data;
        List<Contact> contacts =
            data.map((json) => Contact.fromJson(json)).toList();
        return contacts;
      } else {
        throw Exception('Failed to load contacts');
      }
    } catch (e) {
      throw Exception('Error: $e');
    }
  }

  Future<Contact> fetchContactById(
      String baseUrl, String endpoint, String id) async {
    try {
      dio.options.headers[Api.headerType] = Api.headerValue;

      final response = await dio.get('$baseUrl$endpoint/$id');

      if (response.statusCode == 200) {
        return Contact.fromJson(response.data);
      } else {
        throw Exception('Failed to load contact detail');
      }
    } catch (e) {
      throw Exception('Error: $e');
    }
  }

  Future<List<Contact>> searchContactByName(
      String baseUrl, String endpoint, String name) async {
    try {
      dio.options.headers[Api.headerType] = Api.headerValue;

      final response = await dio.get('$baseUrl$endpoint$name');

      if (response.statusCode == 200) {
        List<dynamic> data = response.data;
        List<Contact> contactSearchResults =
            data.map((json) => Contact.fromJson(json)).toList();
        return contactSearchResults;
      } else {
        throw Exception('Failed to search data');
      }
    } catch (e) {
      if (e is DioException) {
        return [];
      } else {
        throw Exception('Error: $e');
      }
    }
  }

  Future<List<Contact>> fetchContactByEmail(
      String baseUrl, String endpoint, String email) async {
    try {
      dio.options.headers[Api.headerType] = Api.headerValue;

      final response = await dio.get('$baseUrl$endpoint$email');

      if (response.statusCode == 200) {
        List<dynamic> data = response.data;
        List<Contact> contactResults =
            data.map((json) => Contact.fromJson(json)).toList();
        return contactResults;
      } else {
        throw Exception('Failed to load contact detail');
      }
    } catch (e) {
      if (e is DioException) {
        return [];
      } else {
        throw Exception('Error: $e');
      }
    }
  }

  Future<List<Contact>> filterContactByGender(
      String baseUrl, String endpoint, String gender) async {
    try {
      dio.options.headers[Api.headerType] = Api.headerValue;

      final response = await dio.get('$baseUrl$endpoint$gender');

      if (response.statusCode == 200) {
        List<dynamic> data = response.data;
        List<Contact> contactFilterResults =
            data.map((json) => Contact.fromJson(json)).toList();

        return contactFilterResults;
      } else {
        throw Exception('Failed to filter data');
      }
    } catch (e) {
      throw Exception('Error: $e');
    }
  }
}
