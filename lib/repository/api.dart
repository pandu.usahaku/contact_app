class Api {
  // baseUrl person_1
  static const baseUrl1 = 'https://6591052f8cbbf8afa96bd6d4.mockapi.io';
  // base Url person_2
  static const baseUrl2 = 'https://6591068a8cbbf8afa96bd82e.mockapi.io';
  // base Url person_3
  static const baseUrl3 = 'https://659106f88cbbf8afa96bd8fe.mockapi.io';

  static const headerType = 'content-type'; // header type
  static const headerValue = 'application/json'; // header value

  static const personData1 = '/source1/person_1'; // 100 data kontak pertama
  static const personData2 = '/source2/person_2'; // 100 data kontak kedua
  static const personData3 = '/source3/person_3'; // 100 data kontak ketiga

  // Search query endpoint dari baseUrl pertama
  static const searchPerson1 = '/source1/person_1?name=';
  // Search query endpoint dari baseUrl kedua
  static const searchPerson2 = '/source2/person_2?name=';
  // Search query endpoint dari baseUrl ketiga
  static const searchPerson3 = '/source3/person_3?name=';

  // Gender filter endpoint dari baseUrl pertama
  static const filterPerson1 = '/source1/person_1?gender=';
  // Gender filter endpoint dari baseUrl kedua
  static const filterPerson2 = '/source2/person_2?gender=';
  // Gender filter endpoint dari baseUrl ketiga
  static const filterPerson3 = '/source3/person_3?gender=';

  // get data by email endpoint dari baseUrl pertama
  static const emailPerson1 = '/source1/person_1?email=';
  // get data by email endpoint dari baseUrl kedua
  static const emailPerson2 = '/source2/person_2?email=';
  // get data by email endpoint dari baseUrl ketiga
  static const emailPerson3 = '/source3/person_3?email=';
}
