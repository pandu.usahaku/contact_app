import 'package:contact_app/bloc/contact_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContactDetailByEmail extends StatefulWidget {
  const ContactDetailByEmail({super.key});

  @override
  State<ContactDetailByEmail> createState() => _ContactDetailByEmailState();
}

class _ContactDetailByEmailState extends State<ContactDetailByEmail> {
  late ContactBloc _contactBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final String? contactEmail =
        ModalRoute.of(context)!.settings.arguments as String?;

    _contactBloc = BlocProvider.of<ContactBloc>(context);
    _contactBloc.add(FetchContactByEmail(contactEmail!));
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return PopScope(
      onPopInvoked: (didPop) {
        if (didPop) {
          _contactBloc.add(FetchContact());
        }
      },
      child: Scaffold(
        appBar: AppBar(),
        body: BlocBuilder<ContactBloc, ContactState>(builder: (context, state) {
          if (state is ContactDetailByEmailLoading) {
            return Center(
              child: SizedBox(
                height: screenHeight / 20,
                child: const CircularProgressIndicator(),
              ),
            );
          } else if (state is ContactDetailByEmailLoaded) {
            return ListView.builder(
              itemCount: state.contact.length,
              itemBuilder: (context, index) {
                final String avatarImg = state.contact[index].avatar;
                final String name = state.contact[index].name;
                final String gender = state.contact[index].gender;
                final String phoneNumber = state.contact[index].phone;
                final String email = state.contact[index].email;
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      flex: 3,
                      child: SizedBox(
                        width: double.infinity,
                        child: Image.network(
                          avatarImg,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    buildContactData(const Icon(Icons.person), name),
                    buildContactData(const Icon(Icons.wc_outlined), gender),
                    buildContactData(const Icon(Icons.phone), phoneNumber),
                    buildContactData(const Icon(Icons.email), email),
                  ],
                );
              },
            );
          } else if (state is ContactDetailByEmailError) {
            return Center(
              child: Text(state.errorMessage),
            );
          } else {
            return const Center(
              child: Text('Unknown state'),
            );
          }
        }),
      ),
    );
  }
}

Widget buildContactData(Icon icon, String data) => Flexible(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Container(
          decoration: const BoxDecoration(
            border: Border(
              bottom: BorderSide(width: 0.5),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Flexible(
                flex: 1,
                child: Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  child: icon,
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  child: Text(data),
                ),
              ),
            ],
          ),
        ),
      ),
    );
