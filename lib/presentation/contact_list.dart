import 'package:contact_app/bloc/contact_bloc.dart';
import 'package:contact_app/model/contact.dart';
import 'package:contact_app/widget/contact_list_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContactList extends StatefulWidget {
  const ContactList({super.key});

  @override
  State<ContactList> createState() => _ContactListState();
}

class _ContactListState extends State<ContactList> {
  late ContactBloc _contactBloc;
  bool _onSearch = false;
  bool _showCisgender = false;
  String source = '';
  final ScrollController _scrollController = ScrollController();
  final _searchInputController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _contactBloc = BlocProvider.of<ContactBloc>(context);
    _contactBloc.add(FetchContact());

    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _contactBloc.close();
    super.dispose();
  }

  void _onScroll() {
    if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent &&
        !_contactBloc.loadingData) {
      _contactBloc.add(FetchMoreContact());
    }
  }

  void _searchList(value) {
    _contactBloc.add(SearchContactByName(value));
  }

  void _toggleFilter(bool value) {
    if (value) {
      _contactBloc.add(const FilterContactByGender('Cisgender'));
    } else {
      _contactBloc.add(const FilterContactByGender(''));
      _contactBloc.add(FetchContact());
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return PopScope(
      canPop: true,
      onPopInvoked: (didPop) {
        setState(() {
          _contactBloc.add(FetchContact());
        });
      },
      child: Scaffold(
          appBar: AppBar(
            title: _onSearch
                ? SizedBox(
                    width: double.infinity,
                    height: screenHeight / 20,
                    child: TextField(
                      controller: _searchInputController,
                      onSubmitted: _searchList,
                      decoration: const InputDecoration(
                        hintText: 'search a name here....',
                        disabledBorder: InputBorder.none,
                      ),
                    ),
                  )
                : const Text('MyContact'),
            actions: [
              _onSearch
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          _onSearch = false;
                        });
                      },
                      icon: const Icon(Icons.close))
                  : IconButton(
                      onPressed: () {
                        setState(() {
                          _onSearch = true;
                        });
                      },
                      icon: const Icon(Icons.search),
                    ),
            ],
          ),
          drawer: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                const DrawerHeader(
                  child: Text('Filter Options'),
                ),
                ListTile(
                  title: const Text('Cisgender'),
                  trailing: Switch(
                    value: _showCisgender,
                    onChanged: (value) {
                      setState(() {
                        _showCisgender = value;
                        _toggleFilter(value);
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          body: _searchInputController.text.isNotEmpty
              ? BlocBuilder<ContactBloc, ContactState>(
                  builder: (context, state) {
                    return buildSearchState(screenHeight, state);
                  },
                )
              : BlocBuilder<ContactBloc, ContactState>(
                  builder: (context, state) {
                    if (state is FilterLoading ||
                        state is FilterLoaded ||
                        state is FilterError) {
                      return buildFilterState(screenHeight, state);
                    } else {
                      return buildContactState(screenHeight, state);
                    }
                  },
                )),
    );
  }

  Widget buildSearchState(double screenHeight, ContactState state) {
    if (state is SearchLoading) {
      return Center(
        child: SizedBox(
          height: screenHeight / 20,
          child: const CircularProgressIndicator(),
        ),
      );
    } else if (state is SearchLoaded) {
      return buildSearchLoadedState(state.contacts);
    } else if (state is SearchError) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Failed to load data'),
            ElevatedButton(
              onPressed: () {
                _contactBloc.add(FetchContact());
              },
              child: const Text('Reload'),
            ),
          ],
        ),
      );
    } else {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  Widget buildSearchLoadedState(List<Contact> contacts) {
    return ListView.builder(
      itemCount: contacts.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            final String contactEmail = contacts[index].email;
            Navigator.of(context).pushReplacementNamed(
                '/contact-detail-by-email',
                arguments: contactEmail);
          },
          child: ContactListItem(
            avatarImg: contacts[index].avatar,
            name: contacts[index].name,
            phoneNumber: contacts[index].phone,
          ),
        );
      },
    );
  }

  Widget buildContactState(double screenHeight, ContactState state) {
    if (state is ContactLoading) {
      return Center(
        child: SizedBox(
          height: screenHeight / 20,
          child: const CircularProgressIndicator(),
        ),
      );
    } else if (state is ContactLoaded) {
      String? source = state.source;
      return buildContactLoadedState(screenHeight, state.contacts, source);
    } else if (state is ContactError) {
      return Center(
        child: Text(state.errorMessage),
      );
    } else {
      return const Center(
        child: Text('Unknown state'),
      );
    }
  }

  Widget buildContactLoadedState(
      double screenHeight, List<Contact> contacts, String? source) {
    return ListView.builder(
      controller: _scrollController,
      itemCount: contacts.length,
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            final String contactId = contacts[index].id;
            Navigator.of(context).pushNamed('/contact-detail',
                arguments: {'id': contactId, 'source': source});
          },
          child: ContactListItem(
            avatarImg: contacts[index].avatar,
            name: contacts[index].name,
            phoneNumber: contacts[index].phone,
          ),
        );
      },
    );
  }
}

Widget buildFilterState(double screenHeight, ContactState state) {
  if (state is FilterLoading) {
    return Center(
      child: SizedBox(
        height: screenHeight / 20,
        child: const CircularProgressIndicator(),
      ),
    );
  } else if (state is FilterLoaded) {
    return buildFilterLoadedState(state.contacts);
  } else if (state is FilterError) {
    return Center(
      child: Text(state.errorMessage),
    );
  } else {
    return const Center(
      child: Text('Unknown state'),
    );
  }
}

Widget buildFilterLoadedState(List<Contact> contacts) {
  return ListView.builder(
    itemCount: contacts.length,
    itemBuilder: (context, index) {
      return InkWell(
        onTap: () {
          final String contactEmail = contacts[index].email;
          Navigator.of(context)
              .pushNamed('/contact-detail-by-email', arguments: contactEmail);
        },
        child: ContactListItem(
          avatarImg: contacts[index].avatar,
          name: contacts[index].name,
          phoneNumber: contacts[index].phone,
        ),
      );
    },
  );
}
