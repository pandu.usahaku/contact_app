import 'package:contact_app/bloc/contact_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ContactDetail extends StatefulWidget {
  const ContactDetail({super.key});

  @override
  State<ContactDetail> createState() => _ContactDetailState();
}

class _ContactDetailState extends State<ContactDetail> {
  String contactId = "";
  late String source;
  late ContactBloc _contactBloc;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final args =
        ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    contactId = args['id'];
    source = args['source'];

    _contactBloc = BlocProvider.of<ContactBloc>(context);
    _contactBloc.add(FetchContactById(id: contactId, source: source));
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;

    return PopScope(
      onPopInvoked: ((didPop) {
        if (didPop) {
          _contactBloc.add(FetchContact());
        }
      }),
      child: Scaffold(
        appBar: AppBar(),
        body: BlocBuilder<ContactBloc, ContactState>(builder: (context, state) {
          if (state is ContactDetailLoading) {
            return Center(
              child: SizedBox(
                height: screenHeight / 20,
                child: const CircularProgressIndicator(),
              ),
            );
          } else if (state is ContactDetailLoaded) {
            final String avatarImg = state.contact.avatar;
            final String name = state.contact.name;
            final String gender = state.contact.gender;
            final String phoneNumber = state.contact.phone;
            final String email = state.contact.email;

            return Column(
              children: [
                Expanded(
                  flex: 3,
                  child: SizedBox(
                    width: double.infinity,
                    child: Image.network(
                      avatarImg,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                buildContactData(const Icon(Icons.person), name),
                buildContactData(const Icon(Icons.wc_outlined), gender),
                buildContactData(const Icon(Icons.phone), phoneNumber),
                buildContactData(const Icon(Icons.email), email),
              ],
            );
          } else if (state is ContactDetailError) {
            return Center(
              child: Text(state.errorMessage),
            );
          } else {
            return const Center(
              child: Text('Unknown state'),
            );
          }
        }),
      ),
    );
  }
}

Widget buildContactData(Icon icon, String data) => Flexible(
      flex: 1,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Container(
          decoration: const BoxDecoration(
            border: Border(
              bottom: BorderSide(width: 0.5),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Flexible(
                flex: 1,
                child: Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  child: icon,
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  margin: const EdgeInsets.only(bottom: 10),
                  child: Text(data),
                ),
              ),
            ],
          ),
        ),
      ),
    );
