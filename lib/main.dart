import 'package:contact_app/bloc/contact_bloc.dart';
import 'package:contact_app/presentation/contact_detail.dart';
import 'package:contact_app/presentation/contact_detail_by_email.dart';
import 'package:contact_app/presentation/contact_list.dart';
import 'package:contact_app/repository/contact_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  final ContactData contactData = ContactData();
  final ContactBloc contactBloc = ContactBloc(contactData);

  runApp(MyApp(contactBloc: contactBloc));
}

class MyApp extends StatelessWidget {
  final ContactBloc contactBloc;

  const MyApp({super.key, required this.contactBloc});

  // This widget is the root of application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => contactBloc,
      child: MaterialApp(
          title: 'MyContact',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
            useMaterial3: true,
          ),
          initialRoute: '/',
          routes: {
            '/': (context) => const ContactList(),
            '/contact-detail': (context) => const ContactDetail(),
            '/contact-detail-by-email': (context) =>
                const ContactDetailByEmail(),
          }),
    );
  }
}
